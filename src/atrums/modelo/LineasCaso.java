package atrums.modelo;

public class LineasCaso {
	private String idLinea;
	private String nombreCaso;
	private String nombreTecnico;
	private Boolean estadoLinea;
	private String comentario;
	
	
	
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	public Boolean getEstadoLinea() {
		return estadoLinea;
	}
	public void setEstadoLinea(Boolean estadoLinea) {
		this.estadoLinea = estadoLinea;
	}
	public String getIdLinea() {
		return idLinea;
	}
	public void setIdLinea(String idLinea) {
		this.idLinea = idLinea;
	}
	public String getNombreCaso() {
		return nombreCaso;
	}
	public void setNombreCaso(String nombreCaso) {
		this.nombreCaso = nombreCaso;
	}
	public String getNombreTecnico() {
		return nombreTecnico;
	}
	public void setNombreTecnico(String nombreTecnico) {
		this.nombreTecnico = nombreTecnico;
	}
	
	
}
