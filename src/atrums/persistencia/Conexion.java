package atrums.persistencia;

import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.apache.tomcat.dbcp.dbcp.BasicDataSource;

import atrums.util.Configuracion;

public class Conexion {
	static final Logger log = Logger.getLogger(Conexion.class);
	private Configuracion configuracion;
	
	public Conexion() {
		this.configuracion = new Configuracion();
	}
	
	public DataSource crearConexionPostgresOpenbravo(){
		BasicDataSource basicDataSource = new BasicDataSource();
		DataSource dataSource = null;
		
		try {
			basicDataSource.setDriverClassName("org.postgresql.Driver");
			basicDataSource.setUsername(this.configuracion.getUsuarioservidor());
			basicDataSource.setPassword(this.configuracion.getPasswordservidor());
			basicDataSource.setUrl("jdbc:postgresql://" + 
					this.configuracion.getIpsevidor() + ":" + 
					this.configuracion.getPuertoservidor() + "/" + 
					this.configuracion.getBddsevidor());
			basicDataSource.setMaxActive(2);
			basicDataSource.setMinIdle(1);
			basicDataSource.setMaxIdle(2);
			basicDataSource.setInitialSize(2);
			
			dataSource = basicDataSource;
			
		} catch (Exception ex) {log.warn(ex.getMessage());}
		
		return dataSource;
	}
}
