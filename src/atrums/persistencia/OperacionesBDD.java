package atrums.persistencia;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import atrums.modelo.CasosAbiertos;
import atrums.modelo.LineasCaso;
import atrums.util.Configuracion;

public class OperacionesBDD {
	static final Logger log = Logger.getLogger(OperacionesBDD.class);
	private Configuracion configuracion = new Configuracion();
	
	public OperacionesBDD() {
		super();
	}
	
	public Connection getConneccion(DataSource dataSource) {
		// TODO Auto-generated method stub
		Connection connection = null;
		
		try {
			connection = dataSource.getConnection();
			connection.setAutoCommit(false);
			
			if(connection.isClosed()){
				connection = null;
			}
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		}
		
		return connection;
	}
	
	public boolean loggin(String usuario, String password, Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		boolean loggin = false;
		
		try {
			ResultSet rs = null;
			statement = connection.createStatement();
			
			OperacionesAuxiliares auxiliares = new OperacionesAuxiliares();
			password = auxiliares.encryptSh1(password);
			
			sql = "SELECT count(*) as total "
					+ "FROM ad_user ad "
					+ "WHERE ad.username = '" + usuario + "' AND ad.password = '" + password + "' AND ad.isactive = 'Y';";
			rs = statement.executeQuery(sql);
			
			while (rs.next()){
				if(rs.getInt("total") == 1){
					loggin = true;
				}
			}
			
			rs.close();
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close(); } catch (Exception e) {};
		}
		return loggin;
	}
	
	public String idUser(String usuario, String password, Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		String idusuario= "";
		
		try {
			ResultSet rs = null;
			statement = connection.createStatement();
			
			OperacionesAuxiliares auxiliares = new OperacionesAuxiliares();
			password = auxiliares.encryptSh1(password);
			
			sql = "SELECT ad.c_bpartner_id as idusuario "
					+ "FROM ad_user ad "
					+ "WHERE ad.username = '" + usuario + "' AND ad.password = '" + password + "' AND ad.isactive = 'Y' limit 1;";
			rs = statement.executeQuery(sql);
			
			while (rs.next()){
					idusuario = rs.getString("idusuario");
			}
			
			rs.close();
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close(); } catch (Exception e) {};
		}
		return idusuario;
	}
	
	
	public ArrayList<CasosAbiertos> obtenerCasos(String idUsuario, Connection connection) {
		
		ArrayList<CasosAbiertos> casosAbiertos = new ArrayList<CasosAbiertos>();
		Statement statement = null;
		String sql;
		boolean loggin;
		int numregistros=0;
		
		
		try {	
			ResultSet rs = null;
			statement = connection.createStatement();
			sql="SELECT count(c.neo_caso_id) as numregistros" + 
					" FROM neo_caso c" + 
					" LEFT JOIN neo_casoline l on (c.neo_caso_id=l.neo_caso_id)" + 
					" LEFT JOIN c_bpartner p on (p.c_bpartner_id=c.cbp_cliente_id)" + 
					" LEFT JOIN neo_agencia ag on (ag.neo_agencia_id=c.canaltercero)" + 
					" LEFT JOIN neo_canaltercero ct on (ct.neo_canaltercero_id=ag.canal)" + 
					" LEFT JOIN neo_agencialinea al on (c.agenciacanal=al.neo_agencialinea_id)" + 
					" LEFT JOIN c_bpartner_location cpl on (al.cbp_location_id=cpl.c_bpartner_location_id)" + 
					" WHERE l.cbp_tecnico_id='" + idUsuario + "'";
			
			rs = statement.executeQuery(sql);
						
			while (rs.next()){
				numregistros=rs.getInt("numregistros");
			}
			rs.close();
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close(); } catch (Exception e) {};
		}
		
		
		
		try {	
			ResultSet rs = null;
			statement = connection.createStatement();
			sql="SELECT  c.neo_caso_id as idcaso,"+
					"	 c.documentno as numdocumento," + 
					"    p.name as cliente," + 
					"    c.nombrecaso as nombrecaso," + 
					"    coalesce(ct.nombre_canal,'--') as nombrecanal," + 
					"    coalesce(cpl.name,'--') as agencia," + 
					"    coalesce(c.descripcioncaso,'--') as descripcion"+
					" FROM neo_caso c" + 
					" LEFT JOIN neo_casoline l on (c.neo_caso_id=l.neo_caso_id)" + 
					" LEFT JOIN c_bpartner p on (p.c_bpartner_id=c.cbp_cliente_id)" + 
					" LEFT JOIN neo_agencia ag on (ag.neo_agencia_id=c.canaltercero)" + 
					" LEFT JOIN neo_canaltercero ct on (ct.neo_canaltercero_id=ag.canal)" + 
					" LEFT JOIN neo_agencialinea al on (c.agenciacanal=al.neo_agencialinea_id)" + 
					" LEFT JOIN c_bpartner_location cpl on (al.cbp_location_id=cpl.c_bpartner_location_id)" + 
					" WHERE l.cbp_tecnico_id='" + idUsuario + "' AND l.estadolinea <> 'FIN' GROUP BY 1,3,5,6";
			
			rs = statement.executeQuery(sql);
			
			int i=0;

			while (rs.next()){
				CasosAbiertos caso= new CasosAbiertos();
				caso.setNumDocumento( rs.getString("numdocumento").toString());
				caso.setNombreCliente( rs.getString("cliente").toString());
				caso.setNombreCaso(rs.getString("nombrecaso").toString());
				caso.setNombreCanal( rs.getString("nombrecanal").toString());
				caso.setNombreAgencia( rs.getString("agencia").toString());
				caso.setDescripcionCaso( rs.getString("descripcion").toString());
				caso.setIdCaso(rs.getString("idcaso").toString());
				casosAbiertos.add(caso);
				i++;
			}
			
			rs.close();
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close(); } catch (Exception e) {};
		}
		return casosAbiertos;
	}
	

	public ArrayList<LineasCaso> obtenerLineas(String idTecnico, String idCaso, Connection connection) {
		
		ArrayList<LineasCaso> lineasCaso = new ArrayList<LineasCaso>();
		Statement statement = null;
		String sql;	
		
		try {	
			ResultSet rs = null;
			statement = connection.createStatement();
			sql="SELECT c.nombrecaso as nombrecaso," + 
					"       l.neo_casoline_id as idlinea," + 
					"       p.name as nombretecnico," + 
					"       (case when l.estadolinea='BOR' then True else False end) as estadolinea," + 
					"       COALESCE(l.comentarios,'--') as comentario" +
					" FROM neo_casoline l" + 
					" INNER JOIN neo_caso c on (c.neo_caso_id=l.neo_caso_id)" + 
					" INNER JOIN c_bpartner p on (l.cbp_tecnico_id=p.c_bpartner_id)" + 
					" WHERE  l.cbp_tecnico_id='"+ idTecnico +"'" + 
					" AND c.neo_caso_id='"+idCaso+"'" + 
					" AND l.estadolinea<> 'FIN'";
			
			rs = statement.executeQuery(sql);

			while (rs.next()){
				
				LineasCaso caso= new LineasCaso();
				caso.setIdLinea(rs.getString("idlinea").toString());
				caso.setNombreCaso(rs.getString("nombrecaso").toString());
				caso.setNombreTecnico(rs.getString("nombretecnico").toString());
				caso.setEstadoLinea(rs.getBoolean("estadolinea"));
				caso.setComentario(rs.getString("comentario"));

				lineasCaso.add(caso);

			}
			
			rs.close();
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close(); } catch (Exception e) {};
		}
		return lineasCaso;
	}
	
	public String iniciarLineaCaso(String idLinea, Connection connection){
		Statement statement = null;
		int registro = 0;
		
		String mensaje = "";
		
		try {
				
			String sql;
			statement = connection.createStatement();
			sql = "update neo_casoline" + 
					" set estadolinea='INI'," + 
					"    finicio=TO_DATE(NOW())" + 
					" where neo_casoline_id='"+ idLinea +"'";
				
				registro = statement.executeUpdate(sql);
				
				if(registro == 1){
					mensaje = "Inicio Exitoso";
					connection.commit();
				}else{
					mensaje = "No se pudo Iniciar el Temporizador";
					connection.rollback();
				}
		
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			mensaje = "Inicio no registrado, error: " + ex.getMessage();
			try {connection.rollback();} catch (SQLException e) {}
		} finally {
			try { if (statement != null) statement.close(); } catch (Exception e) {};
		}

		return mensaje;
	}
	
	public String finLineaCaso(String idLinea, String comentario, Connection connection){
		Statement statement = null;
		String sql;
		String mensaje = "";
		String respuesta = "";
		try {
				
			ResultSet rs = null;
			statement = connection.createStatement();
			sql = "select neo_caso_fintiempo_app('"+idLinea+"','"+comentario+"')  as respuesta";
				
			rs = statement.executeQuery(sql);
				
			while (rs.next()){
				respuesta = (rs.getString("respuesta").toString());
			}
			
			if(respuesta.equals("True")){
				mensaje = "Finalizacion Exitosa";
				connection.commit();
			}else{
				mensaje = "No se pudo Finalizar el Temporizador";
				connection.rollback();
			}
			rs.close();
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			mensaje = "Finaizacion no registrada, error: " + ex.getMessage();
			try {connection.rollback();} catch (SQLException e) {}
		} finally {
			try { if (statement != null) statement.close(); } catch (Exception e) {};
		}

		return mensaje;
	}

}
