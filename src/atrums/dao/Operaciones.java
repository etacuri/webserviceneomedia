package atrums.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.apache.log4j.Logger;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;

import atrums.modelo.CasosAbiertos;
import atrums.modelo.LineasCaso;
import atrums.persistencia.Conexion;
import atrums.persistencia.OperacionesBDD;
import javax.sql.DataSource;

@Path("/")
public class Operaciones {
	static final Logger log = Logger.getLogger(Operaciones.class);
	private Conexion dataconexion = new Conexion();
	private DataSource dataSourceOpenbravo = null;
	
	public Operaciones() {
		if(this.dataSourceOpenbravo == null){
			this.dataSourceOpenbravo = dataconexion.crearConexionPostgresOpenbravo();
		}
	}
	
	
	@POST
	@Path("ingresoapp")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	public String loginApp(String inputJsonObj){
		String usuario = "";
		String password="";
		
		JSONObject auxmensaje = new JSONObject();
		
		try {
			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);			
			
			usuario = (String) auxInputJsonObj.get("user");
			password = (String) auxInputJsonObj.get("password");
			
			auxmensaje = login(usuario,password);
			
		} catch (JSONException ex) {
			
			log.warn(ex.getMessage());
			
			try {
			
				auxmensaje.put("warn", ex.getMessage());
			} catch (JSONException e) {
			
				e.printStackTrace();
			}
		}
		
		return auxmensaje.toString();
	}
	
	
	
	
	@POST
	@Path("casosabierto")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	public String buscarCasosAbiertos(String inputJsonObj){
		String idtecnico = "";
				
		//String mensaje = "<?xml version=\"1.0\"?>";
		
		JSONObject auxmensaje = new JSONObject();
		
		try {
			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);			
			
			idtecnico = (String) auxInputJsonObj.get("idtecnico");

			auxmensaje = buscarCasosJson(idtecnico);
			
		} catch (JSONException ex) {
			
			log.warn(ex.getMessage());
			
			try {
			
				auxmensaje.put("warn", ex.getMessage());
			} catch (JSONException e) {
			
				e.printStackTrace();
			}
		}
		
		return auxmensaje.toString();
	}
	
	
	@POST
	@Path("lineascaso")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	public String buscarLineasCasos(String inputJsonObj){
		String idtecnico = "";
		String idcaso = "";
				
		//String mensaje = "<?xml version=\"1.0\"?>";
		
		JSONObject auxmensaje = new JSONObject();
		
		try {
			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);			
			
			idtecnico = (String) auxInputJsonObj.get("idtecnico");
			idcaso = (String) auxInputJsonObj.get("idcaso");

			auxmensaje = buscarLineaCasosJson(idtecnico,idcaso);
			
		} catch (JSONException ex) {
			
			log.warn(ex.getMessage());
			
			try {
			
				auxmensaje.put("warn", ex.getMessage());
			} catch (JSONException e) {
			
				e.printStackTrace();
			}
		}
		
		return auxmensaje.toString();
	}
	
	
	@PUT
	@Path("registrarinicio")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String guardarPedidoPutJson(String inputJsonObj){
		String idlinea = "";
		
		
		JSONObject auxmensaje = new JSONObject();
		
		try {
			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);			

			idlinea = (String) auxInputJsonObj.get("idlinea");
		
			
			auxmensaje = registrarInicio(idlinea);
			auxmensaje.put("type", true);
		} catch (JSONException ex) {
		
			log.warn(ex.getMessage());
		
			try {
				auxmensaje.put("warn", ex.getMessage());
				auxmensaje.put("type", false);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
		return auxmensaje.toString();
	}
	
	@PUT
	@Path("registrarfin")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String guardarInicioPutJson(String inputJsonObj){
		String idlinea = "";
		String comentario="";
		
		
		JSONObject auxmensaje = new JSONObject();
		
		try {
			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);			

			idlinea = (String) auxInputJsonObj.get("idlinea");
			comentario = (String) auxInputJsonObj.get("comentario");
		
			
			auxmensaje = registrarFin(idlinea,comentario);
			auxmensaje.put("type", true);
		} catch (JSONException ex) {
		
			log.warn(ex.getMessage());
		
			try {
				auxmensaje.put("warn", ex.getMessage());
				auxmensaje.put("type", false);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
		return auxmensaje.toString();
	}
	
	
	
	
	/*
	 * 
	 */
	
	
	private JSONObject buscarCasosJson(String idusuario){
		
		boolean continuar = true;
		
		JSONObject auxRespuesta = new JSONObject();
		JSONObject auxSeguridad = new JSONObject();
		
		try{
			if(idusuario.equals("")){
				auxSeguridad.put("usuario", "No hay usuario");
				continuar = false;
			}else{
				auxSeguridad.put("usuario", idusuario);
			}
			
			
			auxRespuesta.put("usuario", idusuario);
			
			if(continuar){
				Connection conHome = null;
				
				try{
					OperacionesBDD home = new OperacionesBDD();
					conHome = home.getConneccion(dataSourceOpenbravo);
					
					if(conHome == null){
						
						auxRespuesta.put("respuesta", "No hay conexi�n con la BDD de Openbravo, comun�quese con el administrador");
						
					}else{
						
						ArrayList<CasosAbiertos> casosAbiertos = new ArrayList<CasosAbiertos>();
						
						casosAbiertos=home.obtenerCasos(idusuario, conHome);
						
						// poner la parte para convertir a json
						
						List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
						
					    Map<String, Object> map;
						
						 JSONArray jsArray = new JSONArray();
						 
						 for (int i = 0; i < casosAbiertos.size(); i++) {
							 map = new HashMap<String, Object>();
						        map.put("numcaso", casosAbiertos.get(i).getNumDocumento());
						        map.put("nombrecaso",casosAbiertos.get(i).getNombreCaso());
						        map.put("nombrecliente",casosAbiertos.get(i).getNombreCliente());
						        map.put("nombrecanal",casosAbiertos.get(i).getNombreCanal());
						        map.put("nombreagencia",casosAbiertos.get(i).getNombreAgencia());
						        map.put("descrpcion",casosAbiertos.get(i).getDescripcionCaso());
						        map.put("idcaso", casosAbiertos.get(i).getIdCaso());
						        list.add(map);
						        jsArray.put(map);
					      }
						 
						auxRespuesta.put("casos",jsArray);						
					}
				}catch (Exception ex) {
					log.warn(ex.getMessage());
				}finally {
					try {if(conHome != null)conHome.close();} catch (SQLException ex) {}
				}
			}else{
				auxRespuesta.put("respuesta", "Verifique los datos");
			}
		} catch (JSONException e) {
			log.warn(e.getMessage());
		}
		
		return auxRespuesta;
	}
	
	
	
	/*
	 * 
	 * 
	 * 
	 * */
	

	/*
	 * 
	 */
	
	
	private JSONObject buscarLineaCasosJson(String idtecnico, String idcaso){
		
		boolean continuar = true;
		
		JSONObject auxRespuesta = new JSONObject();
		JSONObject auxSeguridad = new JSONObject();
		
		try{
			if(idtecnico.equals("")){
				auxSeguridad.put("usuario", "No hay usuario");
				continuar = false;
			}else if(idtecnico.equals("")){
					auxSeguridad.put("caso", "No hay usuario");
					continuar = false;
				}else{
				auxSeguridad.put("tecnico", idtecnico);
			}
			
				
			if(continuar){
				Connection conHome = null;
				
				try{
					OperacionesBDD home = new OperacionesBDD();
					conHome = home.getConneccion(dataSourceOpenbravo);
					
					if(conHome == null){
						
						auxRespuesta.put("respuesta", "No hay conexi�n con la BDD de Openbravo, comun�quese con el administrador");
						
					}else{
						
						ArrayList<LineasCaso> lineaCaso = new ArrayList<LineasCaso>();
						
						lineaCaso=home.obtenerLineas(idtecnico,idcaso, conHome);
						
						// poner la parte para convertir a json
						
						List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
						
					    Map<String, Object> map;
						
						 JSONArray jsArray = new JSONArray();
						 
						 for (int i = 0; i < lineaCaso.size(); i++) {
							 map = new HashMap<String, Object>();
						        map.put("nombrecaso",lineaCaso.get(i).getNombreCaso());
						        map.put("idlinea",lineaCaso.get(i).getIdLinea());
						        map.put("nombretecnico",lineaCaso.get(i).getNombreTecnico());
						        map.put("estadolinea", lineaCaso.get(i).getEstadoLinea());
						        map.put("comentario", lineaCaso.get(i).getComentario());
						        list.add(map);
						        jsArray.put(map);
					      }
						 
						auxRespuesta.put("casos",jsArray);
					    System.out.println(auxRespuesta.toString());
						
					}
				}catch (Exception ex) {
					log.warn(ex.getMessage());
				}finally {
					try {if(conHome != null)conHome.close();} catch (SQLException ex) {}
				}
			}else{
				auxRespuesta.put("respuesta", "Verifique los datos");
			}
		} catch (JSONException e) {
			log.warn(e.getMessage());
		}
		
		return auxRespuesta;
	}
	
	
	
	/*
	 * 
	 * 
	 * 
	 * */
	
private JSONObject registrarInicio(String idcaso){
		
		boolean continuar = true;
		
		JSONObject auxRespuesta = new JSONObject();
		JSONObject auxSeguridad = new JSONObject();
		
		try{
			if(idcaso.equals("")){
				auxSeguridad.put("Linea", "No Linea");
				continuar = false;
			}else{
				auxSeguridad.put("Linea", idcaso);
			}
			
			
			
			if(continuar){
				Connection conHome = null;
				
				try{
					OperacionesBDD home = new OperacionesBDD();
					conHome = home.getConneccion(dataSourceOpenbravo);
					
					if(conHome == null){
						
						auxRespuesta.put("respuesta", "No hay conexi�n con la BDD de Openbravo, comun�quese con el administrador");
						
					}else{
						
						
						
						String casosAbiertos=home.iniciarLineaCaso(idcaso, conHome);
						
						// poner la parte para convertir a json
						
						
						 
						auxRespuesta.put("resultado",casosAbiertos);

						
					}
				}catch (Exception ex) {
					log.warn(ex.getMessage());
				}finally {
					try {if(conHome != null)conHome.close();} catch (SQLException ex) {}
				}
			}else{
				auxRespuesta.put("respuesta", "Error al registrar");
			}
		} catch (JSONException e) {
			log.warn(e.getMessage());
		}
		
		return auxRespuesta;
	}
	/*
	 * 
	 * 
	 */
private JSONObject registrarFin(String idcaso, String comentario){
	
	boolean continuar = true;
	
	JSONObject auxRespuesta = new JSONObject();
	JSONObject auxSeguridad = new JSONObject();
	
	try{
		if(idcaso.equals("")){
			auxSeguridad.put("Linea", "No Linea");
			continuar = false;
		}else{
			auxSeguridad.put("Linea", idcaso);
		}
		
		
		
		if(continuar){
			Connection conHome = null;
			
			try{
				OperacionesBDD home = new OperacionesBDD();
				conHome = home.getConneccion(dataSourceOpenbravo);
				
				if(conHome == null){
					
					auxRespuesta.put("respuesta", "No hay conexi�n con la BDD de Openbravo, comun�quese con el administrador");
					
				}else{
					
					
					
					String finLinea=home.finLineaCaso(idcaso,comentario, conHome);
					
					// poner la parte para convertir a json
					
					
					 
					auxRespuesta.put("resultado",finLinea);
					
				}
			}catch (Exception ex) {
				log.warn(ex.getMessage());
			}finally {
				try {if(conHome != null)conHome.close();} catch (SQLException ex) {}
			}
		}else{
			auxRespuesta.put("respuesta", "Error al actualizar");
		}
	} catch (JSONException e) {
		log.warn(e.getMessage());
	}
	
	return auxRespuesta;
}
/*
 * 
 * 
 * 
 */
	
	
	
	
	
private JSONObject login(String usuario,String password){
		
		boolean continuar = true;
		
		JSONObject auxRespuesta = new JSONObject();
		JSONObject auxSeguridad = new JSONObject();
		
		try{
			if(usuario.equals("")){
				auxSeguridad.put("usuario", "No hay usuario");
				continuar = false;
			}else if(password.equals("")){
				auxSeguridad.put("password", "No hay password");
				continuar = false;
			}else{
				auxSeguridad.put("usuario", usuario);
			}
			
			
			auxRespuesta.put("usuario", usuario);
			
			if(continuar){
				Connection conHome = null;
				
				try{
					OperacionesBDD home = new OperacionesBDD();
					conHome = home.getConneccion(dataSourceOpenbravo);
					
					if(conHome == null){
						auxRespuesta.put("respuesta", "No hay conexi�n con la BDD de Openbravo, comun�quese con el administrador");
						
					}else{
						
						boolean casosResultado;
						
						casosResultado=home.loggin(usuario, password, conHome);
						auxRespuesta.put("verificacion", casosResultado);
						
						if (casosResultado) {
							String idusuario=home.idUser(usuario, password, conHome);
							auxRespuesta.put("idusuario", idusuario);
						}else {
							auxRespuesta.put("idusuario", "N/A");
						}
					    
						
					}
				}catch (Exception ex) {
					log.warn(ex.getMessage());
				}finally {
					try {if(conHome != null)conHome.close();} catch (SQLException ex) {}
				}
			}else{
				auxRespuesta.put("respuesta", "Verifique los datos");
			}
		} catch (JSONException e) {
			log.warn(e.getMessage());
		}
		
		return auxRespuesta;
	}
		
}
