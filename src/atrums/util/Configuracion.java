package atrums.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.apache.log4j.Logger;

public class Configuracion {
	static final Logger log = Logger.getLogger(Configuracion.class);
	private InputStream confService = null;
	Properties propiedad = new Properties();
	
	private String ipsevidor;
	private String puertoservidor;
	private String usuarioservidor;
	private String passwordservidor;
	private String bddsevidor;
	
	private String docpedido;
	private String pagoterm;
	private String tarifa;
	private String metodopago;
	
	public Configuracion() {
		try {
			this.confService = this.getClass().getClassLoader().getResourceAsStream("./..//Configuracion/configuracion.xml");
			this.propiedad.loadFromXML(confService);
			
			this.ipsevidor = this.propiedad.getProperty("ipservidor");
			this.puertoservidor = this.propiedad.getProperty("puertoservidor");
			this.usuarioservidor = this.propiedad.getProperty("usuariosservidor");
			this.passwordservidor = this.propiedad.getProperty("passwordservidor");
			this.bddsevidor = this.propiedad.getProperty("bddservidor");
			
			this.docpedido = this.propiedad.getProperty("docpedido");
			this.pagoterm = this.propiedad.getProperty("pagoterm");
			this.tarifa = this.propiedad.getProperty("tarifa");
			this.metodopago = this.propiedad.getProperty("metodopago");
			
			this.propiedad.clear();
			this.confService.close();
		} catch (IOException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		}
	}

	public String getMetodopago() {
		return metodopago;
	}

	public void setMetodopago(String metodopago) {
		this.metodopago = metodopago;
	}

	public String getTarifa() {
		return tarifa;
	}

	public void setTarifa(String tarifa) {
		this.tarifa = tarifa;
	}

	public String getPagoterm() {
		return pagoterm;
	}

	public void setPagoterm(String pagoterm) {
		this.pagoterm = pagoterm;
	}

	public String getDocpedido() {
		return docpedido;
	}

	public void setDocpedido(String docpedido) {
		this.docpedido = docpedido;
	}

	public Properties getPropiedad() {
		return propiedad;
	}

	public void setPropiedad(Properties propiedad) {
		this.propiedad = propiedad;
	}

	public String getIpsevidor() {
		return ipsevidor;
	}

	public void setIpsevidor(String ipsevidor) {
		this.ipsevidor = ipsevidor;
	}

	public String getPuertoservidor() {
		return puertoservidor;
	}

	public void setPuertoservidor(String puertoservidor) {
		this.puertoservidor = puertoservidor;
	}

	public String getUsuarioservidor() {
		return usuarioservidor;
	}

	public void setUsuarioservidor(String usuarioservidor) {
		this.usuarioservidor = usuarioservidor;
	}

	public String getPasswordservidor() {
		return passwordservidor;
	}

	public void setPasswordservidor(String passwordservidor) {
		this.passwordservidor = passwordservidor;
	}

	public String getBddsevidor() {
		return bddsevidor;
	}

	public void setBddsevidor(String bddsevidor) {
		this.bddsevidor = bddsevidor;
	}
}
